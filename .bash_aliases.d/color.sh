#!/usr/bin/bash
#
# Aliases for color.
# https://github.com/devonjones/bash_magic/blob/master/bash_aliases.d/color.sh

if [ "$TERM" != "dumb" ] && [ -x /usr/bin/dircolors ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi
