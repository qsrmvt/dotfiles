#!/usr/bin/bash
#
# Aliases for todo.txt

if command -v todo.sh &> /dev/null
then
    alias t='todo.sh'
    alias tls='todo.sh ls'
    alias tpri='todo.sh pri'
    alias tadd='todo.sh add'
    alias tproj='todo.sh view project'
    alias tcon='todo.sh view context'
    alias tdate='todo.sh -x view date'
    alias trm='todo.sh rm'
    alias tarc='todo.sh archive'
    alias tdo='todo.sh do'
    alias tsch='todo.sh schedule'
fi
