#!/usr/bin/bash
#
# Aliases for dotfiles

alias dotfiles='git --git-dir=$HOME/.config/dotfiles --work-tree=$HOME'
