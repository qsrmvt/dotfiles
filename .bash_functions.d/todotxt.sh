#!/usr/bin/bash
#
# Functions for todo.txt

if command -v todo.sh &> /dev/null
then
    ta () {
        local date="$(date +%Y-%m-%d)"
        todo.sh add "${date} ${*}"
    }
fi
