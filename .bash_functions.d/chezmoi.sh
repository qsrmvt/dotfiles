#!/usr/bin/bash
#
# Do a thing for chezmoi.

if ! command -v chezmoi &> /dev/null
then
    echo chezmoi is missing!

    sh -c "$(wget -qO- git.io/chezmoi) -b ~/.scripts/"
fi
