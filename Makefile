break:
	@echo This is a safeguard. Try \"make init\" instead.

init: ~/.bash-it/bash_it.sh
	if ! command -v chezmoi &> /dev/null ; then sh -c "$(wget -qO- git.io/chezmoi)" ; fi

~/.bash-it/bash_it.sh:
	git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash-it
