" Auto-install Junegunn's vim-plug
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vim-plug Plugin
call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'scrooloose/Syntastic'
Plug 'kien/ctrlp.vim'
Plug 'Chiel92/vim-autoformat'
" Plug 'blueyed/vim-ansible-yaml'
Plug 'pearofducks/ansible-vim'
Plug 'w0rp/ale'
" COLORSCHEMES
Plug 'morhetz/gruvbox'
call plug#end()

" Ansible-vim Settings
let g:ansible_unindent_after_newline = 1
let g:ansible_attribute_highlight = "ob"
let g:ansible_name_highlight = 'b'
let g:ansible_extra_keywords_highlight = 1

" Airline Settings
let g:airline_powerline_fonts = 0


" Syntastic Settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set cc=80

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

nnoremap <leader>eo :lopen<CR>
nnoremap <leader>ec :lclose<CR>

let g:syntastic_css_checkers = ['csslint']
let g:syntastic_html_checkers = ['tidy']
let g:syntastic_yaml_checkers = ['jsyaml']

" CtrlP Settings
let g:ctrlp_cmd = 'CtrlPBuffer'

" General Settings
syntax on
set showmatch
filetype on
filetype plugin indent on
set encoding=utf-8
set nocompatible

" Appearance Settings
set background=dark
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
let g:airline_theme='gruvbox'
highlight Normal ctermbg=NONE

set number
set scrolloff=2
set fillchars=vert:\

" Bell
set noerrorbells
set visualbell

" Tab Settings
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=4
set backspace=indent,eol,start
set autoindent
set smarttab
set smartindent
set pastetoggle=<F2>

" Searching and History Settings
set nohlsearch
set incsearch
set ignorecase
set smartcase
set nobackup
set noswapfile
set history=50

" Other settings
set shortmess+=I
set nolazyredraw
set spelllang=en

set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc

" Keybinds
nnoremap <leader>v :source $MYVIMRC<CR>
nnoremap <leader>n :lnext<CR>
nnoremap <F5> :make<CR>
nnoremap <F4> :Autoformat<CR>
nnoremap <F3> :set spell<CR>
nnoremap <leader>p p=']
