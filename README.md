# [qsrmvt](https://gitlab.com/qsrmvt/)'s Dotfiles

## Contents

* [The Method](#the-method)
* [Managed Dots](#managed-dots)

## The Method

These dotfiles are managed with a combination of Atlassian's method (["The best way to store your dotfiles: A bare Git repository"](https://www.atlassian.com/git/tutorials/dotfiles)) which I poached from [Wabri](https://github.com/Wabri/dotfiles), and [chezmoi](https://www.chezmoi.io/).

The repository itself is handled by the alias `alias dots='git --git-dir=$HOME/.config/dotfiles --work-tree=$HOME'`, while the templating is handled by chezmoi using the files in `~/.local/share/chezmoi/` and `~/.config/chezmoi/chezmoi.toml`.

Initial setup should look like:

```bash
git clone --bare https://gitlab.com/qsrmvt/dotfiles.git ~/.config/dotfiles
git --git-dir=$HOME/.config/dotfiles --work-tree=$HOME checkout
make init
chezmoi -v apply
```

This repository also makes use of [pre-commit](https://pre-commit.com/) for sanity checking files to be checked in.

## Managed Dots

* Terminal: [alacritty](https://github.com/alacritty/alacritty)
	* Shell: bash with [Bash-it](https://github.com/Bash-it/bash-it)
	* Editor: [Neovim](https://neovim.io/)
	* File manager: [Ranger](https://github.com/ranger/ranger)
	* Tmux
